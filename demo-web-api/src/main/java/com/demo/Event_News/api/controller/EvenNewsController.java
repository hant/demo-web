package com.demo.Event_News.api.controller;

import com.demo.Event_News.api.dto.EventsNewsResDto;
import com.demo.Event_News.domain.service.EventNewService;
import com.demo.Event_News.domain.service.IEventNewService;
import com.demo.common.rest.response.BaseResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class EvenNewsController {
    private final IEventNewService iEventNewService;
    @GetMapping("/events/news")
    BaseResponse<Page<EventsNewsResDto>> getAll(Pageable pageable){
        return BaseResponse.ofSucceeded(iEventNewService.getAll(pageable));
    }

}

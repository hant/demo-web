package com.demo.Event_News.api.dto;

import com.demo.Event_News.domain.model.Type;
import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class EventNewRequestDto extends TimestampEntity {

    String title;

    String desc;

    String imgUrl;

    Type rank;
}

package com.demo.Event_News.api.dto;

import com.demo.Event_News.domain.model.Type;
import com.demo.event.domain.model.Event;
import com.demo.shared.model.JpaIDEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventsNewsResDto extends JpaIDEntity {

    String title;

    String desc;

    String imgUrl;

    Type rank;

    Event event;
}

package com.demo.Event_News.domain.mapper;

import com.demo.Event_News.api.dto.EventNewRequestDto;
import com.demo.Event_News.domain.model.EventNews;
import com.demo.common.mapper.IObjectMapper;
import com.demo.event.api.dto.EventRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface IEventNewsRequestDtoMapper extends IObjectMapper<EventNewRequestDto, EventNews> {
    @Mapping(target = "id",ignore = true)
    EventNews merge(@MappingTarget EventNews target,EventNewRequestDto source);
}

package com.demo.Event_News.domain.mapper;

import com.demo.Event_News.api.dto.EventsNewsResDto;
import com.demo.Event_News.domain.model.EventNews;
import com.demo.common.mapper.IObjectMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IEventsNewsResDtoMapper extends IObjectMapper<EventsNewsResDto,EventNews> {
}

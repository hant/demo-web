package com.demo.Event_News.domain.model;

import com.demo.event.domain.model.Event;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class EventEventNews {
    @EmbeddedId
    private EventEventNewsId id;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("eventId")
    @JsonBackReference
    private Event event;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("eventNewId")
    @JsonBackReference
    private EventNews eventNews;

    public EventEventNews(Event event, EventNews eventNews) {
        this.id= new EventEventNewsId(event.getId(),eventNews.getId());
        this.event = event;
        this.eventNews = eventNews;
    }

    public EventEventNews() {

    }
}

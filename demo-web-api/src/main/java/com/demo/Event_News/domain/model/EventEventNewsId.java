package com.demo.Event_News.domain.model;

import com.demo.shared.model.JpaIDEntity;
import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;
@Embeddable
@Data
public class EventEventNewsId implements Serializable {
    private Long eventId;
    private Long eventNewId;

    public EventEventNewsId(Long eventId, Long eventNewId) {
        this.eventId = eventId;
        this.eventNewId = eventNewId;
    }

    public EventEventNewsId() {
    }
}

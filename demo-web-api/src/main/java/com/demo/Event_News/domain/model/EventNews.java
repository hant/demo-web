package com.demo.Event_News.domain.model;

import com.demo.event.domain.model.Event;
import com.demo.shared.model.JpaIDEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity@Table(name="events_news")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventNews extends JpaIDEntity {
    String title;

    @Column(name = "`desc`")
    String desc;

    @Column(name="img_Url")
    String imgUrl;

    @Column(name = "`rank`", nullable = false)
    String rank;

    @Enumerated(EnumType.STRING)
    Type type;


    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    Event event;
}

package com.demo.Event_News.domain.service;

import com.demo.Event_News.api.dto.EventNewRequestDto;
import com.demo.Event_News.api.dto.EventsNewsResDto;
import com.demo.Event_News.domain.model.EventNews;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EventNewService {
    Page<EventsNewsResDto> getAll(Pageable pageable);
    EventsNewsResDto getById(Long id);
    boolean deleteById(Long id);
    EventNews insert(EventNewRequestDto eventNewRequestDto);
    EventNews updateById(Long id,EventNewRequestDto eventNewRequestDto);
}

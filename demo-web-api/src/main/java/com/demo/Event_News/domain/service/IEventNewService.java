package com.demo.Event_News.domain.service;

import com.demo.Event_News.api.dto.EventNewRequestDto;
import com.demo.Event_News.api.dto.EventsNewsResDto;
import com.demo.Event_News.domain.mapper.IEventNewsRequestDtoMapper;
import com.demo.Event_News.domain.mapper.IEventsNewsResDtoMapper;
import com.demo.Event_News.domain.model.EventNews;
import com.demo.Event_News.repository.EventsNewsRepository;
import com.demo.common.exception.BusinessException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.demo.common.exception.DefaultErrorCode.DEFAULT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class IEventNewService implements EventNewService {
    private final IEventsNewsResDtoMapper iEventsNewsResDtoMapper;
    private final EventsNewsRepository eventsNewsRepository;
    private final IEventNewsRequestDtoMapper iEventNewsRequestDtoMapper;

    @Override
    public Page<EventsNewsResDto> getAll(Pageable pageable) {
        Page<EventNews> eventNews = eventsNewsRepository.findAll(pageable);
        return eventNews.map(iEventsNewsResDtoMapper::to);
    }

    @Override
    public EventsNewsResDto getById(Long id) {
        EventNews eventNews = eventsNewsRepository.findById(id).orElseThrow(() -> new BusinessException(DEFAULT_NOT_FOUND));
        return iEventsNewsResDtoMapper.to(eventNews);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            eventsNewsRepository.deleteById(id);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public EventNews insert(EventNewRequestDto eventNewRequestDto) {
        EventNews eventNews = iEventNewsRequestDtoMapper.from(eventNewRequestDto);

        return eventsNewsRepository.save(eventNews);
    }

    @Override
    public EventNews updateById(Long id, EventNewRequestDto eventNewRequestDto) {
        EventNews exited = eventsNewsRepository.findById(id).orElseThrow(() -> new BusinessException(DEFAULT_NOT_FOUND));
        EventNews update = iEventNewsRequestDtoMapper.merge(exited, eventNewRequestDto);
        return eventsNewsRepository.save(update);
    }
}

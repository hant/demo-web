package com.demo.Event_News.repository;

import com.demo.Event_News.domain.model.EventNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventsNewsRepository extends JpaRepository<EventNews,Long> {

}

package com.demo.Event_Speaker.api.Controller;

import com.demo.Event_Speaker.api.Dto.EventSpeakerRequestDto;
import com.demo.Event_Speaker.api.Dto.EventSpeakerResDto;
import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.Event_Speaker.domain.service.EventSpeakerService;
import com.demo.common.rest.response.BaseResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class EventSpeakerController {
    private final EventSpeakerService eventSpeakerService;
    @GetMapping("/speaker")
    BaseResponse<Page<EventSpeakerResDto>> getAll(Pageable pageable) throws ExecutionException, InterruptedException {
        return BaseResponse.ofSucceeded(eventSpeakerService.getAll(pageable).get());
    }
    @GetMapping("/speaker/{id}")
    BaseResponse<EventSpeakerResDto> getById(@PathVariable Long id) throws ExecutionException, InterruptedException {
        return BaseResponse.ofSucceeded(eventSpeakerService.getById(id).get());
    }
    @DeleteMapping("/speaker/{id}")
    void deleteById(@PathVariable Long id){
        eventSpeakerService.deleteById(id);
    }

    @PostMapping("/speaker")
    public BaseResponse<EventSpeaker> create(@RequestBody EventSpeakerRequestDto eventSpeakerRequestDto){
        return BaseResponse.ofSucceeded(eventSpeakerService.insert(eventSpeakerRequestDto));
    }
}

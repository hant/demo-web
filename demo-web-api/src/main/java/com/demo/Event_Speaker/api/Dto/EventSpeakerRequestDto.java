package com.demo.Event_Speaker.api.Dto;

import com.demo.Event_Speaker.domain.model.Level;
import com.demo.shared.infrastructure.JsonAttributeConverter;
import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventSpeakerRequestDto extends TimestampEntity {

    String name;

    String title;

    String imgUrl;

    String description;

    String phoneNumber;

    String contact;

    String company;

    String major;

    @Convert(converter = JsonAttributeConverter.ListStringConverter.class)
    List<String> projects;

    @Enumerated(EnumType.STRING)
    Level level;

    Long eventId;

}

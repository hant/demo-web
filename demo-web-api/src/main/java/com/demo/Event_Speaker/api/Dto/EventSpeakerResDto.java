package com.demo.Event_Speaker.api.Dto;

import com.demo.Event_Speaker.domain.model.Level;
import com.demo.event.domain.model.Event;
import com.demo.shared.model.JpaIDEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventSpeakerResDto extends JpaIDEntity {

    Long id;

    String name;

    String title;

    String imgUrl;

    String description;

    String contact;

    String company;

    String major;

    List<String> projects;

    Level levelSpeaker;

    Event event;
}

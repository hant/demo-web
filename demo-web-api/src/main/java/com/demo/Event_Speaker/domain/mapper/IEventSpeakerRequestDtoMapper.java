package com.demo.Event_Speaker.domain.mapper;

import com.demo.Event_Speaker.api.Dto.EventSpeakerRequestDto;
import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.common.mapper.IObjectMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface IEventSpeakerRequestDtoMapper extends IObjectMapper<EventSpeakerRequestDto,EventSpeaker> {
    @Mapping(target = "id",ignore = true)
    EventSpeaker merge(@MappingTarget EventSpeaker targe, EventSpeakerRequestDto source);
}

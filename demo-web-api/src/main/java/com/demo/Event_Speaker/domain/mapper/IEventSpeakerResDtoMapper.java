package com.demo.Event_Speaker.domain.mapper;

import com.demo.Event_Speaker.api.Dto.EventSpeakerResDto;
import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.common.mapper.IObjectMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IEventSpeakerResDtoMapper extends IObjectMapper<EventSpeakerResDto, EventSpeaker> {
}

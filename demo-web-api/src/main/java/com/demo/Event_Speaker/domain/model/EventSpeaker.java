package com.demo.Event_Speaker.domain.model;

import com.demo.event.domain.model.Event;
import com.demo.shared.infrastructure.JsonAttributeConverter;
import com.demo.shared.model.JpaIDEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.mapstruct.Mapping;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "events_speaker")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventSpeaker extends JpaIDEntity implements Serializable {

    String name;

    String title;

    @Column(name = "`img_Url`")
    String imgUrl;

    String description;

    @Column(name = "phone_Number")
    String phoneNumber;

    @Column(name = "`contact`")
    String contact;

    String company;

    String major;

    @Convert(converter = JsonAttributeConverter.ListStringConverter.class)
    List<String> projects;

    @Enumerated(EnumType.STRING)
    Level level;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    Event event;

}

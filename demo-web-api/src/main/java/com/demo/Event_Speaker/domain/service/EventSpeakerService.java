package com.demo.Event_Speaker.domain.service;

import com.demo.Event_Speaker.api.Dto.EventSpeakerRequestDto;
import com.demo.Event_Speaker.api.Dto.EventSpeakerResDto;
import com.demo.Event_Speaker.domain.mapper.IEventSpeakerRequestDtoMapper;
import com.demo.Event_Speaker.domain.mapper.IEventSpeakerResDtoMapper;
import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.Event_Speaker.repository.EventSpeakerRepository;
import com.demo.common.exception.BusinessException;
import com.demo.mini.api.dto.response.MiniSpeakerResDto;
import com.demo.mini.api.mapper.MiniSpeakerResDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

import static com.demo.common.exception.DefaultErrorCode.DEFAULT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class EventSpeakerService implements IEventSpeakerService {
    private final EventSpeakerRepository eventSpeakerRepository;
    private final IEventSpeakerResDtoMapper iEventSpeakerResDtoMapper;
    private final IEventSpeakerRequestDtoMapper iEventSpeakerRequestDtoMapper;
    private final MiniSpeakerResDtoMapper miniSpeakerResDtoMapper;

    @Override
    @Async("asyncExecutor")
    public CompletableFuture<Page<EventSpeakerResDto>> getAll(Pageable pageable) {
        Page<EventSpeaker> eventSpeakers = eventSpeakerRepository.findAll(pageable);
        return CompletableFuture.completedFuture(eventSpeakers.map(iEventSpeakerResDtoMapper::to));
    }

    @Override
    @Async("asyncExecutor")
    public CompletableFuture<EventSpeakerResDto> getById(Long id) {
        EventSpeaker eventSpeaker = eventSpeakerRepository.findById(id).orElseThrow(() -> new BusinessException(DEFAULT_NOT_FOUND));
        return CompletableFuture.completedFuture(iEventSpeakerResDtoMapper.to(eventSpeaker));
    }

    @Override
    public Boolean deleteById(Long id) {
        try {
            eventSpeakerRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public EventSpeaker insert(EventSpeakerRequestDto eventSpeakerRequestDto) {
        EventSpeaker eventSpeaker=iEventSpeakerRequestDtoMapper.from(eventSpeakerRequestDto);
       return eventSpeakerRepository.save(eventSpeaker);


    }

    @Override
    public EventSpeaker updateById(Long id, EventSpeakerRequestDto eventSpeakerRequestDto) {
        EventSpeaker exited=eventSpeakerRepository.findById(id).orElseThrow(()-> new BusinessException(DEFAULT_NOT_FOUND));
        EventSpeaker toUpdate=iEventSpeakerRequestDtoMapper.merge(exited,eventSpeakerRequestDto);

        return eventSpeakerRepository.save(toUpdate);
    }

    @Override
    public Page<MiniSpeakerResDto> getMiniSpeaker(Pageable pageable) {
        Page<EventSpeaker> miniSpeakerResDtos=eventSpeakerRepository.findAll(pageable);
        return miniSpeakerResDtos.map(miniSpeakerResDtoMapper::to);
    }


    @Override
    public MiniSpeakerResDto getMiniSpeakerById(Long id) {
        EventSpeaker eventSpeaker=eventSpeakerRepository.findById(id).orElseThrow(()->
                new BusinessException(DEFAULT_NOT_FOUND));


        return miniSpeakerResDtoMapper.to(eventSpeaker);
    }
}

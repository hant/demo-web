package com.demo.Event_Speaker.domain.service;

import com.demo.Event_Speaker.api.Dto.EventSpeakerRequestDto;
import com.demo.Event_Speaker.api.Dto.EventSpeakerResDto;
import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.mini.api.dto.response.MiniSpeakerResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.concurrent.CompletableFuture;

public interface IEventSpeakerService {
     CompletableFuture<Page<EventSpeakerResDto>> getAll(Pageable pageable);
     CompletableFuture<EventSpeakerResDto> getById(Long id);
     Boolean deleteById(Long id);
     EventSpeaker insert(EventSpeakerRequestDto eventSpeakerRequestDto);
     EventSpeaker updateById(Long id, EventSpeakerRequestDto eventSpeakerRequestDto);
     Page<MiniSpeakerResDto> getMiniSpeaker(Pageable pageable);
     MiniSpeakerResDto getMiniSpeakerById(Long id);
}

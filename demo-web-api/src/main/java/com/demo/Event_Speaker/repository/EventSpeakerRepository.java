package com.demo.Event_Speaker.repository;

import com.demo.Event_Speaker.domain.model.EventSpeaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventSpeakerRepository extends JpaRepository<EventSpeaker,Long> {
    List<EventSpeaker> findEventSpeakersByIdIn(List<Long> list);

}

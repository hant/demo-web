package com.demo.coupons.api.Dto;

import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import java.time.Instant;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CouponsRequestDto extends TimestampEntity {

    String name;

    String code;

    String description;

    Instant expireTime;
    Integer quantity;

    Long ticketId;

    Long eventId;

    Double discount;

    Double discountPercentage;

    Double maxDiscount;
    Integer currentQuantity;


}

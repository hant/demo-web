package com.demo.coupons.api.Dto;

import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.Instant;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CouponsResDto extends TimestampEntity {

    Long id;

    String name;

    String description;

    Instant expireTime;

    Integer quantity;

    Long ticketId;

    Long eventId;

    Double discount;

    Double discountPercentage;

    Double maxDiscount;

    Integer currentQuantity;
}

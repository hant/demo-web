package com.demo.coupons.api.controller;

import com.demo.common.rest.response.BaseResponse;
import com.demo.coupons.api.Dto.CouponsRequestDto;
import com.demo.coupons.api.Dto.CouponsResDto;
import com.demo.coupons.domain.model.Coupons;
import com.demo.coupons.domain.service.ICouponsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class CouponsController {
    private final ICouponsService iCouponsService;

    @GetMapping("/coupons")
    public BaseResponse<Page<CouponsResDto>> getAll(Pageable pageable) {
        return BaseResponse.ofSucceeded(iCouponsService.getCoupons(pageable));
    }
    @GetMapping("/coupons/{id}")
    public BaseResponse<CouponsResDto> getById(@PathVariable Long id){
        return BaseResponse.ofSucceeded(iCouponsService.getById(id));
    }
    @DeleteMapping("/coupons/{id}")
    public void deleteById(@PathVariable Long id){
        iCouponsService.deleteById(id);
    }
    @PostMapping("/coupons/create")
    public BaseResponse<Coupons> insert(@RequestBody CouponsRequestDto couponsRequestDto){
        return BaseResponse.ofSucceeded(iCouponsService.insert(couponsRequestDto));
    }
    @PutMapping("/coupons/{id}")
    public BaseResponse<Coupons> update(@PathVariable Long id ,@RequestBody CouponsRequestDto couponsRequestDto){
        return BaseResponse.ofSucceeded(iCouponsService.update(id,couponsRequestDto));
    }
}

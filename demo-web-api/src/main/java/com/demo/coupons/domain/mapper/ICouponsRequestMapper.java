package com.demo.coupons.domain.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.coupons.api.Dto.CouponsRequestDto;
import com.demo.coupons.domain.model.Coupons;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ICouponsRequestMapper extends IObjectMapper<CouponsRequestDto, Coupons> {
    @Mapping(target = "id",ignore = true)
    Coupons merge(@MappingTarget Coupons target,CouponsRequestDto source);
}

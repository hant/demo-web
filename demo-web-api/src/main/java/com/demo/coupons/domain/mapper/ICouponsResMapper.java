package com.demo.coupons.domain.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.coupons.api.Dto.CouponsResDto;
import com.demo.coupons.domain.model.Coupons;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ICouponsResMapper extends IObjectMapper<CouponsResDto, Coupons> {
}

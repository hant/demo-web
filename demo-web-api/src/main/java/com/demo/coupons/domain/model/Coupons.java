package com.demo.coupons.domain.model;

import com.demo.shared.model.JpaIDEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Data
@Entity
@Table(name="coupons")
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Coupons extends JpaIDEntity {
    String name;

    String code;

    String description;

    @Column(name = "expire_time")
    Instant expireTime;

    Integer quantity;

    @Column(name = "ticket_id")
    Long ticketId;

    @Column(name = "event_id")
    Long eventId;

    Double discount;

    @Column(name = "discount_percentage")
    Double discountPercentage;

    @Column(name = "max_discount")
    Double maxDiscount;

    @Column(name = "current_quantity")
    Integer currentQuantity;

    public void setDefaultCode() {
        String string1= RandomStringUtils.randomAlphanumeric(3);
        String string2= String.valueOf(System.currentTimeMillis());
        this.code=string1+string2;
    }
    public void setCurrentQuantity(Integer currentQuantity) {
        this.currentQuantity = currentQuantity;
        this.currentQuantity=quantity;
    }
    public void setDefaultExpireTime(){
        Instant i= Instant.now();
        this.expireTime=i.plus(20, ChronoUnit.DAYS);
    }
}

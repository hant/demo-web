package com.demo.coupons.domain.service;

import com.demo.common.exception.BusinessException;
import com.demo.coupons.api.Dto.CouponsRequestDto;
import com.demo.coupons.api.Dto.CouponsResDto;
import com.demo.coupons.domain.mapper.ICouponsRequestMapper;
import com.demo.coupons.domain.mapper.ICouponsResMapper;
import com.demo.coupons.domain.model.Coupons;
import com.demo.coupons.repository.CouponsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.demo.common.exception.DefaultErrorCode.DEFAULT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class CouponsService implements ICouponsService {
    private final CouponsRepository couponsRepository;

    private final ICouponsResMapper iCouponsResMapper;
    private final ICouponsRequestMapper iCouponsRequestMapper;
    ;

    @Override
    public Page<CouponsResDto> getCoupons(Pageable pageable) {
        Page<Coupons> coupons = couponsRepository.findAll(pageable);
        return coupons.map(iCouponsResMapper::to);
    }

    @Override
    public CouponsResDto getById(Long id) {
        Coupons coupons=couponsRepository.findById(id).orElseThrow(()->new BusinessException(DEFAULT_NOT_FOUND));

        return iCouponsResMapper.to(coupons);
    }

    @Override
    public Boolean deleteById(Long id) {
        try {
            couponsRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public Coupons insert(CouponsRequestDto couponsRequestDto) {
        Coupons coupons= iCouponsRequestMapper.from(couponsRequestDto);
        coupons.setCurrentQuantity(coupons.getQuantity());
        coupons.setDefaultCode();
        coupons.setDefaultExpireTime();
        couponsRepository.save(coupons);
        return coupons;
    }

    @Override
    public Coupons update(Long id, CouponsRequestDto couponsRequestDto) {
        Coupons existed=couponsRepository.findById(id).orElseThrow(()->new BusinessException(DEFAULT_NOT_FOUND));
        Coupons update= iCouponsRequestMapper.merge(existed,couponsRequestDto);
        return couponsRepository.save(update);
    }


}

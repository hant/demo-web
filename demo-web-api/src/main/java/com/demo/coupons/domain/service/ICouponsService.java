package com.demo.coupons.domain.service;

import com.demo.coupons.api.Dto.CouponsRequestDto;
import com.demo.coupons.api.Dto.CouponsResDto;
import com.demo.coupons.domain.model.Coupons;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ICouponsService {
    Page<CouponsResDto> getCoupons(Pageable pageable);
    CouponsResDto getById(Long id);
    Boolean deleteById(Long id);
    Coupons insert(CouponsRequestDto couponsRequestDto);
    Coupons update(Long id,CouponsRequestDto couponsRequestDto);


}

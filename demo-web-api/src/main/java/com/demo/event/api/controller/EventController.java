package com.demo.event.api.controller;

import com.demo.common.rest.response.BaseResponse;
import com.demo.event.api.dto.EventRequestDto;
import com.demo.event.api.dto.EventResDto;
import com.demo.event.domain.model.Event;
import com.demo.event.domain.service.IEventService;
import com.demo.event.infrastructure.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class EventController {
    private final IEventService eventService;
    private final EventRepository eventRepository;

    @GetMapping("/events")
    public BaseResponse<Page<EventResDto>> getAll(Pageable pageable) {
        return BaseResponse.ofSucceeded(eventService.getEvents(pageable));
    }

    @GetMapping("/events/{id}")
    public BaseResponse<EventResDto> getById(@PathVariable Long id) {
        return BaseResponse.ofSucceeded(eventService.getById(id).get());
    }

    @DeleteMapping("/events/{id}")
    public void deEvent(@PathVariable Long id) {
        eventService.delete(id);
    }

    @PutMapping("/events/{id}")
    public BaseResponse<Event> updateById(@PathVariable Long id,
                                          @RequestBody @Valid EventRequestDto eventRequestDto) {
        return BaseResponse.ofSucceeded(eventService.update(id, eventRequestDto));
    }

    @PostMapping("/events")
    public BaseResponse<EventRequestDto> insert(@RequestBody @Valid EventRequestDto eventRequestDto) {
        return BaseResponse.ofSucceeded(eventService.insert(eventRequestDto));
    }
    @DeleteMapping("/events")
    public void deleteAll(){
        eventService.clean();
    }
}

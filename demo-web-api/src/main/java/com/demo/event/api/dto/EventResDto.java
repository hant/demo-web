package com.demo.event.api.dto;

import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.event.domain.model.Event;
import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventResDto extends TimestampEntity {

    Long id;

    String name;

    String desc;

    List<EventSpeaker> listEventSpeaker;
}

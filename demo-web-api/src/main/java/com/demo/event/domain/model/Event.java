package com.demo.event.domain.model;

import com.demo.Event_News.domain.model.EventEventNews;
import com.demo.Event_News.domain.model.EventNews;
import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.shared.model.JpaIDEntity;
import com.demo.ticket.domain.model.Ticket;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "events", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Event extends JpaIDEntity implements Serializable {

    @Column(nullable = false, unique = true)
    @NotEmpty
    String name;

    String slug;

    @Column(name = "`desc`")
    String desc;

    @Column(name = "`start_time`")
    Instant startTime;

    @Column(nullable = false)
    String location;

    String imgUrl;

    Boolean isActive;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<EventSpeaker> listEventSpeaker;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<EventEventNews> listEventNew;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<Ticket> listTicket;

    public void setDefaultSlug() {
        this.setSlug(this.getSlug().toLowerCase().replaceAll(" ", "-"));
    }

    public void addSpeaker(EventSpeaker eventSpeaker) {
        listEventSpeaker.add(eventSpeaker);
        eventSpeaker.setEvent(this);
    }

    public void setListEventSpeaker(List<EventSpeaker> listEventSpeaker) {
        this.listEventSpeaker = listEventSpeaker;
        for (EventSpeaker n:listEventSpeaker
             ) {n.setEvent(this);
        }
    }

    public void removeSpeaker(EventSpeaker eventSpeaker) {
        listEventSpeaker.remove(eventSpeaker);
        eventSpeaker.setEvent(null);
    }
}

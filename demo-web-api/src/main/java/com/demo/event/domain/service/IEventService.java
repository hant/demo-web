package com.demo.event.domain.service;

import com.demo.event.api.dto.EventRequestDto;
import com.demo.event.api.dto.EventResDto;
import com.demo.event.domain.model.Event;
import com.demo.mini.api.dto.response.MiniEventResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IEventService {
    Page<EventResDto> getEvents(Pageable pageable);
    Optional<EventResDto> getById(long id);
    EventRequestDto insert(EventRequestDto eventRequestDto);
    Event update(Long id, EventRequestDto eventRequestDto);
    boolean delete(Long id);
    boolean clean();
    List<MiniEventResDto> getMiniEvent();
}

package com.demo.food.api.controller;

import com.demo.common.rest.response.BaseResponse;
import com.demo.food.api.dto.FoodRequestDto;
import com.demo.food.api.dto.FoodResDto;
import com.demo.food.domain.model.Food;
import com.demo.food.domain.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class FoodController {
    private final FoodService foodService;

    @GetMapping("/foods")
    public BaseResponse<Page<FoodResDto>> getAll(Pageable pageable) {
        return BaseResponse.ofSucceeded(foodService.getAll(pageable));
    }
    @GetMapping("/foods/{id}")
    public BaseResponse<FoodResDto> getById(@PathVariable Long id){
        return BaseResponse.ofSucceeded(foodService.getById(id));
    }
    @PutMapping("/foods/{id}")
    public BaseResponse<Food> updateById(@PathVariable Long id,@RequestBody FoodRequestDto
                                                   foodRequestDto){
        return BaseResponse.ofSucceeded(foodService.update(id,foodRequestDto));
    }
    @PostMapping("/foods")
    public BaseResponse<Food> insert(@RequestBody Food food){
        return BaseResponse.ofSucceeded(foodService.insert(food));
    }
//    @GetMapping("/foods/{name}")
//    public BaseResponse<Page<FoodResDto>> getByName(@PathVariable String name){
//        return BaseResponse.ofSucceeded(foodService.getByName(name));
//    }

}

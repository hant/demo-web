package com.demo.food.api.dto;

import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FoodRequestDto extends TimestampEntity {
    String name;
    Character size;
    String note;
}

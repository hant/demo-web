package com.demo.food.domain.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.food.api.dto.FoodRequestDto;
import com.demo.food.domain.model.Food;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface FoodRequestDtoMapper extends IObjectMapper<Food, FoodRequestDto> {
    @Mapping(target = "id", ignore = true)
    Food merge(@MappingTarget Food target, FoodRequestDto source);
}

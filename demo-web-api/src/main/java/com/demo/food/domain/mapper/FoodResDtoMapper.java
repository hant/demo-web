package com.demo.food.domain.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.food.api.dto.FoodResDto;
import com.demo.food.api.dto.FoodResDto;
import com.demo.food.domain.model.Food;
import org.mapstruct.Mapper;

@Mapper(componentModel ="spring")
public interface FoodResDtoMapper extends IObjectMapper<FoodResDto, Food> {
}

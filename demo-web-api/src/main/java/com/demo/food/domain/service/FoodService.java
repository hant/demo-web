package com.demo.food.domain.service;

import com.demo.common.exception.BusinessException;
import com.demo.food.api.dto.FoodRequestDto;
import com.demo.food.api.dto.FoodResDto;
import com.demo.food.domain.mapper.FoodRequestDtoMapper;
import com.demo.food.domain.mapper.FoodResDtoMapper;
import com.demo.food.domain.model.Food;
import com.demo.food.repository.FoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.demo.common.exception.DefaultErrorCode.DEFAULT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class FoodService implements IFoodService {
    private final FoodRepository repository;
    private final FoodResDtoMapper mapper;

    private final FoodRequestDtoMapper foodRequestDtoMapper;


    @Override
    public Page<FoodResDto> getAll(Pageable pageable) {
        Page<Food> foods = repository.findAll(pageable);

        return foods.map(mapper::to);
    }

    @Override
    public FoodResDto getById(Long id) {
        Food food = repository.findById(id).orElseThrow(() -> new BusinessException(DEFAULT_NOT_FOUND));

        return mapper.to(food);
    }


    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Page<FoodResDto> getByName(String name) {
        List<Food> foods=repository.findAllByName(name);
        return null;
    }

    @Override
    public Food insert(Food food) {
        food.setId(null);
        Food save = repository.save(food);
        return save;
    }

    @Override
    public Food update(Long id, FoodRequestDto foodRequestDto) {
        Food existed = repository.findById(id).orElseThrow(() -> new BusinessException(DEFAULT_NOT_FOUND));
        Food toUpdate = foodRequestDtoMapper.merge(existed, foodRequestDto);
        Food save = repository.save(toUpdate);

        return save;
    }
}

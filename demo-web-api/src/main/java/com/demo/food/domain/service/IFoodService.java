package com.demo.food.domain.service;

import com.demo.food.api.dto.FoodRequestDto;
import com.demo.food.api.dto.FoodResDto;
import com.demo.food.domain.model.Food;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface IFoodService {
    Page<FoodResDto> getAll(Pageable pageable);

    FoodResDto getById(Long id);

    void deleteById(Long id);

    Page<FoodResDto> getByName(String name);
    Food insert(Food food);

    Food update(Long id, FoodRequestDto foodRequestDto);
}

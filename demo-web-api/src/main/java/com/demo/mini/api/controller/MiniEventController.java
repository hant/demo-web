package com.demo.mini.api.controller;

import com.demo.common.rest.response.BaseResponse;
import com.demo.event.domain.service.EventService;
import com.demo.mini.api.dto.response.MiniEventResDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MiniEventController {
    private final EventService eventService;
    @GetMapping("/miniEvent")
    public BaseResponse<List<MiniEventResDto>> getMiniEvent(){
        return BaseResponse.ofSucceeded(eventService.getMiniEvent());
    }

}

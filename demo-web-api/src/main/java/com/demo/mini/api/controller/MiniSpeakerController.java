package com.demo.mini.api.controller;

import com.demo.Event_Speaker.domain.service.IEventSpeakerService;
import com.demo.common.rest.response.BaseResponse;
import com.demo.mini.api.dto.response.MiniSpeakerResDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MiniSpeakerController {
    private final IEventSpeakerService iEventSpeakerService;
    @GetMapping("/miniSpeaker")
    public BaseResponse<Page<MiniSpeakerResDto>> getAll(Pageable pageable){
        return BaseResponse.ofSucceeded(iEventSpeakerService.getMiniSpeaker(pageable));
    }
    @GetMapping("/miniSpeaker/{id}")
    public BaseResponse<MiniSpeakerResDto> getById(@PathVariable Long id){
        return BaseResponse.ofSucceeded(iEventSpeakerService.getMiniSpeakerById(id));
    }
}

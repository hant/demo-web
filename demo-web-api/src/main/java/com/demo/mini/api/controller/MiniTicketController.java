package com.demo.mini.api.controller;

import com.demo.common.rest.response.BaseResponse;
import com.demo.mini.api.dto.response.MiniTicketResDto;
import com.demo.ticket.domain.service.ITicketService;
import com.demo.ticket.domain.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class MiniTicketController {
    private final TicketService ticketService;
    @GetMapping("/miniticket")
    public BaseResponse<Page<MiniTicketResDto>> getMiniTicket(Pageable pageable){
        return BaseResponse.ofSucceeded(ticketService.getTicket(pageable));
    }

}

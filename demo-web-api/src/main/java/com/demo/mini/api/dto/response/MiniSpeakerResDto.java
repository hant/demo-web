package com.demo.mini.api.dto.response;

import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MiniSpeakerResDto extends TimestampEntity {
    Long id;
    String name;
    String level;
}

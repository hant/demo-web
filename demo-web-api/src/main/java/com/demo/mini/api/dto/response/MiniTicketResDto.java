package com.demo.mini.api.dto.response;

import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MiniTicketResDto extends TimestampEntity {
    Long id;
    String name;
}

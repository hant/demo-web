package com.demo.mini.api.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.event.domain.model.Event;
import com.demo.mini.api.dto.response.MiniEventResDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IMiniEventResDtoMapper extends IObjectMapper<MiniEventResDto, Event> {
}

package com.demo.mini.api.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.mini.api.dto.response.MiniTicketResDto;
import com.demo.ticket.domain.model.Ticket;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IMiniTicketResDtoMapper extends IObjectMapper <MiniTicketResDto,Ticket> {
}

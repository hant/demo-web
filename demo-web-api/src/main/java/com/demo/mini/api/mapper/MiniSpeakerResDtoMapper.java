package com.demo.mini.api.mapper;

import com.demo.Event_Speaker.domain.model.EventSpeaker;
import com.demo.common.mapper.IObjectMapper;
import com.demo.mini.api.dto.response.MiniSpeakerResDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MiniSpeakerResDtoMapper extends IObjectMapper<MiniSpeakerResDto, EventSpeaker> {
}

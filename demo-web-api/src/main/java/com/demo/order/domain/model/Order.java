//package com.demo.order.domain.model;
//
//import com.demo.shared.infrastructure.JsonAttributeConverter;
//import com.demo.shared.model.JpaIDEntity;
//import lombok.AccessLevel;
//import lombok.Data;
//import lombok.experimental.FieldDefaults;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.util.List;
//import java.util.Map;
//
//@Data
//@Entity
//@FieldDefaults(level = AccessLevel.PRIVATE)
//public class Order extends JpaIDEntity {
//
//    String name;
//
//    @Enumerated(EnumType.STRING)
//    OrderStatus status;
//
//    String description;
//
//    @Column(name = "account_id")
//    Long accountId;
//
//    @Column(name = "event_id")
//    Long eventId;
//
//    String couponsCode;
//
//    BigDecimal totalAmount;
//
//    Boolean isSentWebinar;
//
//    Boolean isSentMeeting;
//
//    @Convert(converter = JsonAttributeConverter.ListMapDefinitionConverter.class)
//    @Column(name = "order_detail", columnDefinition = "text")
//    List<Map<String, Object>> orderDetail;
//
//
//}

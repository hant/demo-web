package com.demo.order.domain.model;

public enum OrderStatus {
    PROCESSING,CANCELLED
}

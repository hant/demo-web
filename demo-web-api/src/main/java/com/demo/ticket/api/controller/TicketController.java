package com.demo.ticket.api.controller;

import com.demo.common.rest.response.BaseResponse;
import com.demo.ticket.api.dto.TicketRequestDto;
import com.demo.ticket.api.dto.TicketResDto;
import com.demo.ticket.domain.model.Ticket;
import com.demo.ticket.domain.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class TicketController {
    private final TicketService ticketService;
    @GetMapping("/ticket")
    public BaseResponse<Page<TicketResDto>> getAll(Pageable pageable){
        return BaseResponse.ofSucceeded(ticketService.getTickets(pageable));
    }
    @GetMapping("/ticket/{id}")
    public BaseResponse<TicketResDto> getById(@PathVariable Long id){
        return BaseResponse.ofSucceeded(ticketService.getbyId(id).get());

    }
    @PostMapping("/ticket")
    public BaseResponse<Ticket> insert(@RequestBody TicketRequestDto ticket){
        return BaseResponse.ofSucceeded(ticketService.insert(ticket));
    }
    @PutMapping("/ticket")
    public BaseResponse<Ticket> updateById(@PathVariable Long id,
                                           @RequestBody TicketRequestDto ticketRequestDto){
        return BaseResponse.ofSucceeded(ticketService.update(id,ticketRequestDto));
    }
}

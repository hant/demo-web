package com.demo.ticket.api.dto;

import com.demo.event.domain.model.Event;
import com.demo.shared.infrastructure.JsonAttributeConverter;
import com.demo.shared.model.TimestampEntity;
import com.demo.ticket.domain.model.TypeTicket;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Convert;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TicketRequestDto extends TimestampEntity {

    @NotNull
    String name;

    Character rank;

    String address;

    BigDecimal price;

    Instant startTime;

    String note;

    @Enumerated(value = EnumType.STRING)
    TypeTicket ticketType;

    @Convert(converter = JsonAttributeConverter.ListStringConverter.class)
    List<String> meetings;
}

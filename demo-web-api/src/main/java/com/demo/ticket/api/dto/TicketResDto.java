package com.demo.ticket.api.dto;

import com.demo.shared.model.TimestampEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TicketResDto extends TimestampEntity {
    Long id;
    String name;
    Character rank;
    String address;
    BigDecimal price;
    String note;
}

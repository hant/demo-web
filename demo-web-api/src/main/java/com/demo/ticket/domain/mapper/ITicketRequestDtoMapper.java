package com.demo.ticket.domain.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.ticket.api.dto.TicketRequestDto;
import com.demo.ticket.domain.model.Ticket;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ITicketRequestDtoMapper extends IObjectMapper<TicketRequestDto, Ticket> {
    @Mapping(target = "id",ignore = true)
    Ticket merge(@MappingTarget Ticket target,TicketRequestDto source);
}

package com.demo.ticket.domain.mapper;

import com.demo.common.mapper.IObjectMapper;
import com.demo.ticket.api.dto.TicketResDto;
import com.demo.ticket.domain.model.Ticket;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ITicketResDtoMapper extends IObjectMapper<TicketResDto, Ticket> {
}

package com.demo.ticket.domain.model;

import com.demo.event.domain.model.Event;
import com.demo.shared.infrastructure.JsonAttributeConverter;
import com.demo.shared.model.JpaIDEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Entity(name="tickets")
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Ticket extends JpaIDEntity {
    @Column(nullable = false)
    String name;

    @Column(name="`rank`")
    Character rank;

    String address;

    BigDecimal price;

    Instant startTime;

    String note;

    @Enumerated(value = EnumType.STRING)
            @Column(name="ticket_type")
    TypeTicket ticketType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="event_id")
    @JsonBackReference
    Event event;

    @Convert(converter = JsonAttributeConverter.ListStringConverter.class)
    List<String> meetings;
}

package com.demo.ticket.domain.service;

import com.demo.mini.api.dto.response.MiniTicketResDto;
import com.demo.ticket.api.dto.TicketRequestDto;
import com.demo.ticket.api.dto.TicketResDto;
import com.demo.ticket.domain.model.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ITicketService {
    Page<TicketResDto> getTickets(Pageable pageable);
    Optional<TicketResDto> getbyId(long id);
    Ticket insert(TicketRequestDto ticketRequestDto);
    Ticket update(long id, TicketRequestDto ticketRequestDto);
    Boolean deleteById(long id);
    Page<MiniTicketResDto> getTicket(Pageable pageable);

}

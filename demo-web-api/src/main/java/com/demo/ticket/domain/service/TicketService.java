package com.demo.ticket.domain.service;

import com.demo.Event_Speaker.domain.mapper.IEventSpeakerRequestDtoMapper;
import com.demo.common.exception.BusinessException;
import com.demo.mini.api.dto.response.MiniTicketResDto;
import com.demo.mini.api.mapper.IMiniTicketResDtoMapper;
import com.demo.ticket.api.dto.TicketRequestDto;
import com.demo.ticket.api.dto.TicketResDto;
import com.demo.ticket.domain.mapper.ITicketRequestDtoMapper;
import com.demo.ticket.domain.mapper.ITicketResDtoMapper;
import com.demo.ticket.domain.model.Ticket;
import com.demo.ticket.infrastructure.repository.TicketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.demo.common.exception.DefaultErrorCode.DEFAULT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class TicketService implements ITicketService{
    private final TicketRepository repository;
    private final ITicketResDtoMapper mapper;
    private final IMiniTicketResDtoMapper iMiniTicketResDtoMapper;
    private final ITicketRequestDtoMapper iTicketRequestDtoMapper;

    @Override
    public Page<TicketResDto> getTickets(Pageable pageable) {
        Page<Ticket> tickets=repository.findAll(pageable);
        return tickets.map(mapper::to);

    }

    @Override
    public Optional<TicketResDto> getbyId(long id) {
        Optional<Ticket> ticket=repository.findById(id);

        return ticket.map(mapper::to);


    }
    @Override
    public Ticket insert(TicketRequestDto ticketRequestDto) {
        Ticket ticket= iTicketRequestDtoMapper.from(ticketRequestDto);
        return repository.save(ticket);
    }

    @Override
    public Ticket update(long id, TicketRequestDto ticketRequestDto) {
        Ticket existed =repository.findById(id).orElseThrow(()->new BusinessException(DEFAULT_NOT_FOUND));
        Ticket toUpdate= iTicketRequestDtoMapper.merge(existed,ticketRequestDto);
        return repository.save(toUpdate);
    }

    @Override
    public Boolean deleteById(long id) {
        try{repository.deleteById(id);
            return true;}
        catch (Exception e){
        return false;
    }}

    @Override
    public Page<MiniTicketResDto> getTicket(Pageable pageable) {
        Page<Ticket> ticket=repository.findAll(pageable);
        return ticket.map(iMiniTicketResDtoMapper::to);
    }
}

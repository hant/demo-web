DROP TABLE IF EXISTS `food`;

CREATE TABLE food
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    name           VARCHAR(255) NOT NULL,
    size          CHARACTER(10) NOT NULL,
    price          INTEGER(10) NOT NULL,
    note           VARCHAR(255)  NULL,
    `created_date` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `updated_date` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    CONSTRAINT pk_food PRIMARY KEY (id)
);
DROP TABLE IF EXISTS `events_news`;
CREATE TABLE events_news
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    title          VARCHAR(255) NOT NULL,
    `img_Url`      VARCHAR(255) NULL,
    `rank`         VARCHAR(20) NULL,
    `desc`         VARCHAR(255) NULL,
    `created_date` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `updated_date` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    event_id       BIGINT NULL,
    CONSTRAINT pk_events_news PRIMARY KEY (id)
);


DROP TABLE IF EXISTS `events_speaker`;
CREATE TABLE events_speaker
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    name           VARCHAR(255)          NOT NULL,
    title          VARCHAR(255)          NULL,
    `img_Url`      VARCHAR(255)          NULL,
    description    VARCHAR(255)          Null,
    phone_Number   VARCHAR(255)          Null,
    `contact`      VARCHAR(255)          Null,
    `company`      VARCHAR(255)          Null,
    `major`        VARCHAR(255)          Null,
    `level`        VARCHAR(255)          Null,
    `desc`         VARCHAR(255)          NULL,
    `projects`     VARCHAR(255)          NULL,
    `event_id`     BIGINT(20)            NULL,
    `created_date` datetime(3)           NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_date` datetime(3)           NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    CONSTRAINT pk_events_speaker PRIMARY KEY (id)
)
;
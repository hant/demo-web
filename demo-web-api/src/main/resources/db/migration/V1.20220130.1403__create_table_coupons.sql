DROP TABLE IF EXISTS `coupons`;
CREATE TABLE coupons
(
    id                    BIGINT AUTO_INCREMENT NOT NULL,
    name                  VARCHAR(255)          NULL,
    code                  VARCHAR(255)          NULL,
    description           VARCHAR(255)          NULL,
    quantity              INTEGER(255)          NULL,
    `ticket_id`           BIGINT(255)          NULL,
    `event_id`            BIGINT(255)          NULL,
    discount              DOUBLE(255, 10)       NULL,
    `discount_percentage` DOUBLE(255, 10)       NULL,
    `max_discount`        DOUBLE(255, 10)       NULL,
    `current_quantity`    INTEGER(255)          NULL,
    `expire_time`         datetime(3)           NULL,
    `created_date`        datetime(3)           NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_date`         datetime(3)           NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    CONSTRAINT pk_coupons PRIMARY KEY (id)
);
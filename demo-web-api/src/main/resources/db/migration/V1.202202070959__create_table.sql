DROP TABLE IF EXISTS `owner`;

CREATE TABLE owner
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    name           VARCHAR(255) NULL,
    age            VARCHAR(255) NULL,
    img_url        VARCHAR(255) NULL,
    phone_number   VARCHAR(255) NULL,
    id_number      VARCHAR(255) NULL,
    address        VARCHAR(255) NULL,


    `created_date` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `updated_date` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    CONSTRAINT pk_owner PRIMARY KEY (id)
);